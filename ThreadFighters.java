package StockExchangeSimulator;

import java.util.Date;

class ThreadFighters implements Runnable {
    Date date = new Date();
    Thread person;

    ThreadFighters(String name){person = new Thread(this, name);
        person.start();
    }
    @Override
    public void run() {
        System.out.println("Fighter's name - " +person.getName());
        try {
            for (int count=0; count<10; count++){
                Thread.sleep(1000);
                System.out.println(date + " Fighter " + person.getName()+" Kick ");
            }
        } catch (InterruptedException e) {
            System.out.println("Pause - " + person.getName());
        }
        System.out.println("Stop - " + person.getName());
    }
}

