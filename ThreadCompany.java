package StockExchangeSimulator;

import java.util.Date;
import java.util.Random;

class ThreadCompany implements Runnable {
    Date date = new Date();
    Thread company;

    ThreadCompany(String name){company = new Thread(this, name);
        company.start();
    }
    public static int generateRandomPercentVariation(int min, int max, int stockPriceInitial) {
        Random percentVariation = new Random();
        int count = percentVariation.nextInt((max - min) + 1) + min;
        return stockPriceInitial + (stockPriceInitial/100)*count;
    }
    @Override
    public void run() {
        System.out.println("Company name - " +company.getName());
        try {
            for (int count=0; count<5; count++){
                Thread.sleep(5000);
                System.out.println(date+" Company - " + company.getName()+" price for one company share: " +
                generateRandomPercentVariation(-3, 3, 100));
            }
        } catch (InterruptedException e) {
            System.out.println("Pause - " + company.getName());
        }
        System.out.println("Stop - " + company.getName());
    }
}

