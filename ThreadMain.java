package StockExchangeSimulator;

class ThreadMain {
    public static void main(String[] args) throws InterruptedException {
        MyThreadDemoPlus myThreadDemoPlus = new MyThreadDemoPlus();
        myThreadDemoPlus.MyThreadDemoStart();

        ThreadCompany firstCompany = new ThreadCompany("Google");
        ThreadCompany secondCompany = new ThreadCompany("Xiaomi");
        ThreadCompany thirdCompany = new ThreadCompany("General Motors");
        ThreadFighters firstFighter = new ThreadFighters("Liu Kang");
        ThreadFighters secondFighter = new ThreadFighters("Kitana");
        ThreadFighters thirdFighter = new ThreadFighters("Sub Zero");

        firstCompany.run();
        secondCompany.run();
        thirdCompany.run();
        firstFighter.run();
        secondFighter.run();
        thirdFighter.run();

        System.out.println("<<<Fatality>>>");
    }

}